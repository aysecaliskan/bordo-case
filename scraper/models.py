from django.db import models


class ProductImage(models.Model):
    url = models.URLField()


class ProductPrice(models.Model):
    price = models.DecimalField(
        max_digits=20,
        decimal_places=3
    )
    sale_price = models.DecimalField(
        max_digits=20,
        decimal_places=3
    )
    scraped_at = models.DateTimeField()


class Product(models.Model):
    name = models.CharField(
        max_length=255
    )
    category = models.CharField(
        max_length=255
    )
    code = models.CharField(
        max_length=255
    )
    sales_url = models.URLField()

    images = models.ForeignKey(ProductImage, on_delete=models.CASCADE)
    prices = models.ForeignKey(ProductPrice, on_delete=models.CASCADE)


class ScrapeJob(models.Model):
    file = models.FileField()
