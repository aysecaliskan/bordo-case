import os
from django.core.management import base
from django.core.management.base import BaseCommand, CommandError, CommandParser
from bs4 import BeautifulSoup
import requests
from requests.sessions import Session
from scraper.models import ScrapeJob
import re
import json


class Command(BaseCommand):
    help = 'Scrape lottedfs'
    file = ''
    base_url = 'https://eng.lottedfs.com'
    regex = re.compile(
        r"javascript:ga_adltCheckPrdDtlMove\('(\w+)','(\w+)','(\w+)','(\w+)'\)")

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '-o',
            '--output-file',
            type=str,
            default='out.jl',
            help='output file name',
        )

    def handle(self, *args, **options):
        session = requests.Session()
        session.headers = {
            'authority': 'eng.lottedfs.com',
            'pragma': 'no-cache',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'sec-fetch-site': 'none',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-user': '?1',
            'sec-fetch-dest': 'document',
            'accept-language': 'en',
        }

        categories = self._get_categories(session)
        products = self._get_products(session, categories)
        self._output(products, options.get('output_file'))

    def _output(self, products, filename):
        with open(filename, 'w+') as f:
            for product in products:
                f.write(json.dumps(product))
                f.write(os.linesep)

    def _get_products(self, session, categories):
        for category in categories:
            products = self._product_list_parser(session, category.get('url'))
            for product in products:
                yield product

    def _product_list_parser(self, session: Session, page_url):
        product_page_response = session.get(f'{self.base_url}/{page_url}')
        product_page_soup = BeautifulSoup(
            product_page_response.text, 'html.parser')
        for product_li in product_page_soup.find_all('li', {'class': 'productMd'}):
            yield {
                'url': self.__product_link_generate_from_raw(product_li.a.get('href')),
                'name': product_li.a.find('div', {'class': 'product'}).text,
                'price': product_li.a.find('div', {'class': 'discount'}).strong.text,
            }

    def __product_link_generate_from_raw(self, raw_url):
        matches = self.regex.match(raw_url)
        prdNo = matches.group(1)
        prdOptNo = matches.group(2)
        adltPrdYn = matches.group(4)

        return f'/kr/product/productDetail?prdNo={prdNo}&prdOptNo={prdOptNo}&adltPrdYn={adltPrdYn}'

    def _get_categories(self, session: Session):
        category_link = 'https://eng.lottedfs.com/kr/header/getCommDispCateList'
        category_response = session.get(category_link)

        soup = BeautifulSoup(category_response.text, 'html.parser')
        ul_element = soup.find('ul', {'class': 'sub'})
        li_elements = ul_element.find_all('li')
        for li_element in li_elements:
            yield {
                'name': li_element.a.text,
                'url': li_element.a.get('href')
            }
